// NEW APP
import React from 'react';
import {
  Button,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import * as SecureStore from 'expo-secure-store';

const App: () => Node = () => {
  const [key, setKey] = React.useState('');
  const [value, setValue] = React.useState('');
  const [result, setResult] = React.useState('(result)');

  async function save(key, value) {
    await SecureStore.setItemAsync(key, value);
  }

  async function getValueFor(key) {
    let storeResult = await SecureStore.getItemAsync(key);

    if (storeResult) {
      setResult(storeResult);
    } else {
      alert('Invalid key!');
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.mainText}>Save a key/value.</Text>

      <TextInput
        style={styles.textInput}
        placeholder={'Enter a key:'}
        onChangeText={text => setKey(text)}
        value={key}
      />

      <TextInput
        style={styles.textInput}
        placeholder={'Enter a value:'}
        onChangeText={text => setValue(text)}
        value={value}
      />

      <Button
        title="Save"
        onPress={() => {
          save(key, value);
          setKey('');
          setValue('');
        }}
      />

      <Text style={styles.mainText}>Enter your key:</Text>
      <TextInput
        style={styles.textInput}
        onSubmitEditing={event => {
          console.log(event.nativeEvent.text);
          getValueFor(event.nativeEvent.text);
        }}
        placeholder="Enter a key:"
      />

      <Text style={styles.mainText}>{result}</Text>

      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  mainText: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textInput: {
    height: 55,
    borderColor: 'gray',
    borderWidth: 0.5,
    padding: 10,
    margin: 4,
    borderRadius: 20,
  },
});

export default App;
