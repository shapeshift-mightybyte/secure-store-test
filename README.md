# Secure Store Test

This project is a proof of concept testing that the expo-secure-store library works. 

Demonstrating that two completely different apps cannot access the same Secure Storage space on the phone unless their App ID/Bundle IDs are the same - https://drive.google.com/file/d/1MVOkJoPPxCu769Mbdtxdh3_52c5qYlkS/view?usp=sharing
